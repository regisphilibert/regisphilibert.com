<?php $page ="contact";
if($_SESSION['cf_returndata']){
    if(!$_SESSION['cf_returndata']['valid']){
        extract($_SESSION['cf_returndata']);
    }
    else{
        unset($_SESSION['cf_returndata']);
        $errors =array();
    }


}
else{
    $errors= array();
}
?>
<!-- ///////////////////// CONTACT ////////////////////////-->
<div id="contact">
	<?php include "blocks/top.php" ?>
<div class="main">
		<div class="left">
			<div class="text">
                            <?php if($set_lang=="fr"): ?>
                                <p>Je suis disponible en ce moment&nbsp;!</p>
                                <p>Expliquez-moi tout&hellip;</p>
                                <?php else: ?>
				<p>I'm in your reach!</p>
                                <p>Just leave a few words and we can start&hellip;</p>
				<?php endif; ?>
			</div>
			<a href="callto://regisphilibert" class="skype">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">regisphilibert</p>
			</a>
			<a class="mail">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">bonjour &part; regisphilibert.com</p>
			</a>
			<a href="tel:1-514-623-5997" class="phone">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">+1 (514) 623 5997</p>
			</a>
		</div>
		<div class="right">

			<form action="" method="post">
                <input type="hidden" name="locale" value="<?php echo $set_lang ; ?>"/>
				<label for="fName"><?php echo _("NAME")?><span <?php if(in_array('name', $errors)){echo "style='display:block'";}?>>/<?php echo _("That is not a name") ?>/</span></label>
				<input type="text" name="name" id="fName" value="<?php echo $posted_form_data['name'] ;?>"/>
				<label for="fMail"><?php echo _("EMAIL")?><span <?php if(in_array('email', $errors)){echo "style='display:block'";}?>>/<?php echo _("That is not an email") ?>/</span></label>
                <label for="fAge" class="a5648978"><?php echo _("Age")?></label>
                <input type="text" class="a5648978" id="fAge"  name="age" value=""/>
                <input type="email" name="email" id="fMail" value="<?php echo $posted_form_data['email'] ;?>"/>
				<label for="fObject"><?php echo _("OBJECT")?></label>
				<input type="text" name="object" id="fObject" value="<?php echo $posted_form_data['object'] ;?>"/>
				<label for="fMessage"><?php echo _("MESSAGE")?><span <?php if(in_array('message', $errors)){echo "style='display:block'";}?>>/<?php echo _("Please just a few words") ?>/</span></label>
				<textarea name="message" id="fMessage"><?php echo $posted_form_data['message'] ;?></textarea>
				<div class="submit">
					<button type="submit" id="submit_button"><?php echo _("SEND IT")?></button>
					<div class="chevrons"><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span></div>


				</div>
			</form>
			<div class="form_printed">
				<h2><?php echo _("THANK YOU!")?></h2>
				<p class="fName"><span class="title"><?php echo _("NAME")?>: </span><span class="content"></span></p>
				<p class="fMail"><span class="title"><?php echo _("EMAIL")?>: </span><span class="content"></span></p>
				<p class="fObject"><span class="title"><?php echo _("OBJECT")?>: </span><span class="content"></span></p>
				<p class="fMessage"><span class="title"><?php echo _("MESSAGE")?>: </span><span class="content"></span></p>
				<?php if($set_lang=="en"): ?>
				<p style="color:LightCoral;">!This old thing is not working anymore, visit my new contact page <a style="display: inline;position:relative;color:inherit;text-decoration:underline" href="https://regisphilibert.com/contact">here</a>!</p>
				<?php else: ?>
				<p style="color:LightCoral;">!Ce vieux machin ne marche plus, veuillez vous rendre sur ma nouvelle page contact <a style="display: inline;position:relative;color:inherit;" href="https://regisphilibert.com/fr/contact">ici!</a></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php include "blocks/bottom.php" ?>
</div>
<!-- ///////////////////// END ABOUT  ////////////////////////-->
<?php include "blocks/blank.php" ?>