<?php
session_start();
define('WEBROOT', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
require('functions.php');
require('data.php');
if(isset($_GET['p'])){
    $params = rtrim($_GET['p'], '/');
    $params = explode('/', $params);
}
else{
    $params = array();
}

foreach($works_en as $k => $v){
    $works[] = $k;
}
$w_url = 0;
if(isset($params[0])){
    if(in_array($params[0], $works)){
        $w_url = $params[0];
    }
}
$c_langs = array('en', 'fr');
$set_lang = array_intersect($params, $c_langs);
$lang = new rpLocale();
if(count($set_lang)!=1){
$set_lang = $lang->first_language($_SERVER['HTTP_ACCEPT_LANGUAGE'], $c_langs);
}
else{
    $set_lang = $set_lang[0];
}

require('localization.php');

$err_message = $set_lang!="fr"?$errors_en:$errors_fr;
include('template.php');

?>
