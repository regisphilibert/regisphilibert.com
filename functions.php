<?php

class rpLocale{

    public function sort_langs($language_string, $geo=false){//turn the horrible language accept header into a neat array.
        $lang = explode(',', $language_string);
        foreach($lang as $k =>$v){
            if(strstr($v, ';')){
                //if server < php 5.3
                 //$a = explode(';', $v);
                 //$lang[$k] = $a[0];
                //else :
                $lang[$k] = strstr($v, ';', true);
            }
        }
        //the following only if geo spef of the language is not needed. this will turn every la-CO into la and removes duplicate.
        if($geo){
            foreach($lang as $k =>$v){
                if(strstr($v, '-')){
                //if server < php 5.3
                //$a = explode('-', $v);
                //$lang[$k] = $a[0];
                //else :
                    $lang[$k] = strstr($v, '-', true);
                }
            }
            $lang = array_unique($lang);
        }

        return $lang;
    }
    public function first_language($u, $c){//determine which language available in client is first in user accept, return false no client language is found in user accept.
        $u = $this->sort_langs($u, true);
        $langs = array_intersect($u, $c);//compare users and client language, if none found : false
        if(count($langs) > 0){//if 1 found it is the first language.
            if(count($langs) > 1){//if  > 1, determine which is first in list.
            $first = array_keys($langs);
            $first = $first[0];
            $langs = $langs[$first];
            }
            else{
                $langs = array_values($langs);
                $langs = $langs[0];
            }
        }
        else{
            $langs = false;
        }
    return $langs;
    }

    function other_langs($c, $set_lang=string, $string=false){//depending on the set languages, return the left ones.
       $u =  array($set_lang);
       $a = array_diff($c, $u);

       if($string){
        $a = implode(',', $a);
       }
       return $a;
    }

}//end of Locale()

function arrowTop($p, $li){
    switch($p){
        case 'about':
            switch($li){
                case 1:
                    return "arrow";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'works':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "arrow";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'skills':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                case 3:
                    return "arrow";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'contact':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                case 3:
                    return "none";
                break;
                case 4:
                    return "arrow";
                break;
            }

    }
}

function arrowBottom($p, $li){
    switch($p){
        case 'about':
            switch($li){
                case 1:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'works':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'skills':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                case 3:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'contact':
            switch($li){
                default:
                    return "none";
            }

    }
}

function arrowBlank($p, $li){
    switch($p){
        case 'about':
            switch($li){
                case 1:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'works':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'skills':
            switch($li){
                case 1:
                    return "none";
                break;
                case 2:
                    return "none";
                break;
                case 3:
                    return "none";
                break;
                default:
                    return "";
                break;
            }
        break;
        case 'contact':
            switch($li){
                default:
                    return "none";
            }

    }
}
?>