<?php $other_langs = $lang->other_langs($c_langs, $set_lang, true)?>
<ul class="lang">
    <li><a href="<?php echo WEBROOT.$set_lang ?>"><?php echo $set_lang ?></a></li>
    <li><a class="inactive" href="<?php echo WEBROOT; echo $other_langs ?>"><?php echo $other_langs ?></a></li>
</ul>
<div id="menu">
		<div class="left">
			<div class="js-warning">
				<?php if($set_lang=="en"): ?>
                <strong>This is my old website from 2011.</strong> I let it live here for old time's sake. Contact form is animated but not working. Please use the new one <a href="https://regisphilibert.com/contact">here</a>.
            	<?php else: ?>
            		<strong>Ça c'est mon vieux site de 2011.</strong> Je l'heberbe ici en souvenir du bon vieux temps. Le formulaire de contact est animé mais ne fonctionne pas, rendez-vous sur le nouveau <a href="https://regisphilibert.com/fr/contact">ici</a>.
            	<?php endif; ?>
            </div>
                </div>
		<div class="right">
				<div class="nav_box active"><a href="<?php echo WEBROOT ?>#about" title="about"><?php echo _("ABOUT") ?></a></div>
				<div class="nav_box"><a href="<?php echo WEBROOT ?>#works" title="works"><?php echo _("WORKS") ?></a></div>
				<div class="nav_box"><a href="<?php echo WEBROOT ?>#skills" title="skills"><?php echo _("SKILLS") ?></a></div>
				<div class="nav_box"><a href="<?php echo WEBROOT ?>#contact" title="contact"><?php echo _("REACH ME") ?></a></div>
		</div>
</div>
<div class="top_margin">
	<div class="left">
            
        </div>
	<div class="right">
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
	</div>
</div>