////////////////////// LE FORMULAIRE //////////////////////
//////////////////////////////////////////////////////////
$(document).ready(function(){
    
var deviceAgent = navigator.userAgent.toLowerCase();
var iOS = deviceAgent.match(/(iphone|ipad|ipod)/);
var submitButton = $('form').find('[type="submit"]');
    
    $('label span').hide();// Cache les span d'avertissement.
        if(iOS){ var rgMail = new RegExp(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)([ \t]{1})?$/i);}
       //Regex de James Watts and Francisco Jose Martin Moreno http://fightingforalostcause.net/misc/2006/compare-email-regex.php
        else{ var rgMail = new RegExp(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i);}
       //si iOS on tolère un whitespace à la fin du mail car iOS en rajoute toujours un avec son autocomplete !
        var rgName = new RegExp(/^([&'\s]*[A-Z]\S+)+[ \t]?$/i)
        function showError(input){
            input.prev('label').children('span').stop(true,true).fadeOut(50).fadeIn(300);
        }
        function hideError(input){
            input.prev('label').children('span').stop(true,true).fadeOut(300);
        }
        
    submitButton.click(function(){
        $(this).submit(function(){
            return false;
        })
        form = $(this).parents('form');
        valid = true;
          
        $('form input').each(function(){
            var that = $(this);
            that.val = that.val();
            that.id = that.attr('id');
            
            switch(that.id){
                case 'fName':
                    if(!that.val.match(rgName)){
                        showError(that);
                        valid = false;
                    }
                    else{
                        hideError(that);
                        
                    }
                break;
            
                case 'fMail':
                    if(!that.val.match(rgMail)){
                        showError(that);
                        valid = false;
                    }
                    else{
                        hideError(that);
                        
                    }
                break;
                case 'fMessage':
                    if(that.val == ""){
                        showError(that);
                        valid = false;
                    }
                    else{
                        hideError(that);
                    }
                    
            }
            
          })
            
            
            if(valid){
                setTimeout(function(){
                    $('form input, form textarea').each(function(){
                        var that = $(this);
                        that.val = that.val();
                        that.id = that.attr('id');
                        $('.form_printed p.'+that.id+' span.content').html(that.val);
                    })
                    $('form').hide("slide", {direction:'right'}, 500,
                        function(){
                            $('.form_printed').fadeIn(1500);
                        }
                    );  
                }, 500)
            }
    return false;
            
    })//submit click function
                
})