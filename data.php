<?php
//WORKS

$works_fr = array(
    "artpublic"=>array(
        "title" => "Art Public",
        "url" => "artpublic.ville.montreal.qc.ca",
        "intro" => "Une web app full ajax en partie wordpress",
        "content" =>"<p>Pour ces 25 ans, le bureau d’Art Public de Montréal souhaitait un site moderne qui servirait les Montréalais de 7 à geek à 77 ans.
La réalisation a été confiée à mes <a href='http://www.tonton.ca/' target='_blank'>TONTON</a>, qui m’ont a nouveau laissé m’amuser sur du beau projet local!<br>
J'ai du monter sur cet audacieux design un full ajax wordpress, aidé de typeahead.js, masonry.js Google Map Javascript v3 et notre propre mini api pour compiler toutes ces belles oeuvres en une splendide web app !</p>",
        "tools"=>"Wordpress, Google Map API, typehead, NoUiSlider et bien d'autres",
        "share"=>"tout sauf le web design et le project management",
        "ribbon"=>"DEV"
    ),
    "francemutuelle"=>array(
        "title" => "France Mutuelle",
        "url" => "www.francemutuelle.fr",
        "intro" => "Un site pleinement adaptatif et en partie Wordpress",
        "content" =>"<p>Un projet magnifique avec la parfaite <a href='http://www.agence-aristote.fr'>Agence Aristote</a>. J'ai eu la chance de m'occuper de tout le frontend et de la partie wordpress du backend de ce super projet</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + javascript - 100% du frontend",
        "ribbon"=>"DEV"
    ),
    "readytodye"=>array(
        "title" => "Palladium | Prêt à Teindre",
        "url" => "pret-a-teindre.com/",
        "intro" => "Un site statique trés animé pour Palladium",
        "content" =>"<p>C'est toujours gratifiant lorsqu'une grande marque française vous fait confiance. <a target='_blank' href='http://www.palladiumboots.fr'>Palladium</a> lance un concept révolutionnaire de chaussures à teindre soi-même et avait besoin d'un site riche en vidéos et diagrames pour présenter le projet. En bonus, un localisateur de magasin !</p>",
        "tools"=>"jQuery, PHP, mysql, youtube API",
        "share"=>"css/html + javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
    "griffon"=>array(
        "title" => "BIÈRE GRIFFON",
        "url" => "bieregriffon.com",
        "intro" => "Un wordpress responsive avec une touche d'instagram.",
        "content" =>"<p>Le client souhaitait développer son site web mais s'en servir de plateforme de concours pour le lancement. Il a donc fallu développer un plugin de récupération de données Instagram et préparer le site pour l'après.</p>",
        "tools"=>"Wordpress, jQuery, Instagram API",
        "share"=>"css/html + wordpress/javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
    "lavitrine" =>
    array(
        "title" => "BLOGUE LA VITRINE",
        "url" => "blogue.lavitrine.com",
        "intro" => "Un Wordpress responsive pour le blogue de la célèbre billetterie Montrèalaise.",
        "content" =>"<p>Le client via <a style='text-decoration:underline' target='_blank' href='http://www.tonton.ca/'>Tonton</a> souhaitait un wordpress responsive avec de nombreuses animations CSS et Javascript ainsi qu'une identification couleur pour chaque catégorie. Plusieurs widget étaient demandés.
Tonton a fourni un design très class pour un développement à la hauteur !</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + wordpress/javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
    "bleubonbon"=>array(
        "title" => "Bleu Bonbon",
        "url" => "bleubonbon.com",
        "intro" => "Un wordpress responsive pour un projet drôle.",
        "content" =>"<p>Je me suis bien amusé ! Le fondateur de Bleu Bonbon voulait un wordpress adaptatif à la hauteur de ses idées, et de son design. Ajoutons à ça un formulaire à téléversement multiple utilisant la meilleure captcha du moment <a href='http://areyouahuman.com'>Are You A Humane ?</a>. Heureusement, j'étais dans le coin ;)",
        "tools"=>"Wordpress, jQuery, Are You Human",
        "share"=>"css/html + wordpress/javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
    "baron" =>
    array(
        "title" => "BARON",
        "url" => "baronmag.com",
        "intro" => "Un Wordpress responsive pour un magazine très populaire à Montrèal.",
        "content" =>"<p>Le client souhaitait refondre son site actuel avec Wordpress. Il fournissait le design et laissait ses contracteurs le conseiller sur les meilleurs pratiques actuelles. J'ai développé le thème original de A à Z avec les dernières fonctionnalités wordpress. Un client ouvert + un projet sympa = :)</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + wordpress/javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
    "adde" =>
    array(
        "title" => "ADDE",
        "url" => "adde.fr",
        "intro" => "Un site corpo construit avec mon propre CMS!",
        "content" =>"<p>Les CMS populaires ne suffisaient pas pour les besoins spéciaux du client. J'ai donc enfin pu utiliser mon CMS fait maison pour ce site corporatif.
            <br>
            Enrichi d'une chouette carte interactive faite avec Raphael.js, ce site parfaitement designé - Merci <a href='http://atelier1n.com'>Mariane Laforest</a> - sert complètement les besoins du client ! Moi fier et content :)</p>" ,
        "tools"=>"Bakeware (mon cms tricoté main), jQuery, Raphael.js",
        "share"=>"css/html et développement. tout sauf le design !",
        "ribbon"=>"DEV++"
    ),
    "cssoff" =>
    array(
        "title" => "CSSOFF 2012 TOP 25",
        "url" => "unmatchedstyle.com/cssoff/2011/0291/",
        "url_show" => "regardez mon entrée!",
        "intro" => "Je suis arrivé 22ème sur 400+ au concours <a style='text-decoration:underline;color:white;' href='http://unmatchedstyle.com/news/cssoff-winners-2012.php'>CSS OFF</a>",
        "content" =>"<p>Un prix spécial était accordé au 25 premiers.
            Les soumissions devaient être responsives, compatibles ie6-ie7, sans php et bien sur très proches du design original. Tout ça pour obtenir le maximum de points.
            <br>
            Ce n'est pas concrètement du travail rémunéré, juste un truc dont je suis fier et donc je le mets dans mon portfolio.</p>" ,
        "tools"=>"CSS, jQuery",
        "share"=>"css/html + javascript - tout sauf le design",
        "ribbon"=>"DEV"
    ),
/*    "candycookie" =>
    array(
        "title" => "CANDY COOKIE BOULEVARD",
        "url" => "candycookieboulevard.fr/patisseries",
        "url_show" => "candycookieboulevard.fr",
        "intro" => "Site blogging pour un salon de thé restaurant très en vue.",
        "content" =>"<p>Le client souhaitait intégrer son actuelle plateforme de blogging (tumblr). Les postes tumblr sont donc récupérés par xml et intégrés directement dans le code du site. Il fallait aussi récupérer les infos «sociales» et autres publications du clients (dernier statut facebook et autres chiffres).
<br>Autre contrainte : une carte du restaurant interactive mais sans Flash&nbsp;!
                    </p>",
        "tools" => "jQuery, xml et php pour l'intégration.",
        "share" => "css/html + php/javascript &mdash; tout sauf le design.",
        "ribbon"=>"DEV"

    ),*/
    "taxocanada" =>
    array(
        "title" => "TAX-O-CANADA",
        "url" => "taxocanada.com",
        "intro" => "Un calculateur des taxes de vente du Canada avec sa jolie carte.",
        "content" =>"<p>Tax-O-Canada calcule en cours de frappe les taxes de vente par province. L’utilisateur peut changer à tout moment de province sans retaper son montant ni recharger la page.
La langue et la localité de l’utilisateur, donc sa province, sont détectées au chargement.<p>",
        "tools" => "Raphael.js pour la carte interactive et jQuery pour le reste.",
        "share" => "la totalité! (bon sauf le logo)",
        "ribbon"=>"DESIGN + DEV"
    ),
/*    "cheribibi" =>
    array(
        "title" => "LES VIDEOS DE CHERIBIBI",
        "url" => "cheribibi-videos.com",
        "intro" => "Un site de contenu vidéo à la navigation innovante et épurée.",
        "content" =>"<p>Le client voulait une façon innovante d'afficher ses vidéos.
Nous avons donc proposé une seule page présentant la totalité des vidéos.
Pour le confort du spectateur, seule la vidéo en cours de visionnage est affichée.
<br>Au clique sur la plume, un formulaire de contact apparaît.
<br>Un back office a été mis au point pour permettre au client d'ajouter lui-même ses vidéos.</p>",
        "tools" => "jQuery et php",
        "share" => "css/html + php/javascript + back office &mdash;tout sauf le design.",
        "ribbon"=>"DEV"
    ),*/
/*    "iao" =>
    array(
        "title" => "I AM ORGANIZED&trade;",
        "url" => "iamorganized.com",
        "intro" => "Un site virtrine pour un service de clouding innovant !",
        "content" =>"<p>Le client souhaitait un site multilingue simple mais responsive. Il a donc fallu adapter les images/vidéos aux supports mais aussi modifier la navigation grâce à PageSlide.js.</p>",
        "tools" => "jQuery, php, FitVids.js, PageSlide.js",
        "share" => "css/html + php/javascript + design responsive.",
        "ribbon"=>"DEV"
    )*/

/*TEMPLATE =>
    array(
        "title" => "",
        "url" => "",
        "intro" => "",
        "content" =>""
    )
 */

);
$works_en = array(
    "artpublic"=>array(
        "title" => "Art Public",
        "url" => "artpublic.ville.montreal.qc.ca",
        "intro" => "A full ajax web app party wordpress",
        "content" =>"<p>For its 25th birthday, the Public Art bureau of Montreal wanted a modern approach for its website that would appeal to the young and old inhabitants of its oh so artsy city.
The amazing <a href='http://www.tonton.ca/' target='_blank'>TONTON</a>
were entrusted with the project and allowed me once again to have fun on a big scale local project.<br>
It took an amazing design, a full ajax wordpress, mixed with typeahead.js, masonry.js, Google Map Javascript v3 and our own home brewed API to compile this list of artworks into a superb web app !</p>",
        "tools"=>"Wordpress, Google Map API, typehead, NoUiSlider and many more",
        "share"=>"everything but web design and project management",
        "ribbon"=>"DEV"
    ),
    "francemutuelle"=>array(
        "title" => "France Mutuelle",
        "url" => "www.francemutuelle.fr",
        "intro" => "Un full responsive part wordpress site",
        "content"=>"<p>An amazing projet with the oh awesome <a href='http://www.aristote-agency.com/'>Aristote Agency</a>. I was lucky enought to be entrusted with the front end and the wordpress side of the backend of this amazing endeavour.</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + javascript - 100% of frontend",
        "ribbon"=>"DEV"
    ),
    "readytodye"=>array(
        "title" => "Palladium | Ready To Dye",
        "url" => "pret-a-teindre.com",
        "intro" => "A rich static website for Palladium",
        "content" =>"<p>Always flatterring when a big french company trusts you. <a target='_blank' href='http://www.palladiumboots.fr'>Palladium</a> invents the shoe you dye yourself and needed a rich website to present the concept through videos and diagrams. Plus, I made them a convenient store locator.</p>",
        "tools"=>"jQuery, PHP, mysql, youtube API",
        "share"=>"css/html + javascript - everything but web design !",
        "ribbon"=>"DEV"
    ),
    "griffon"=>array(
        "title" => "GRIFFON",
        "url" => "http://bieregriffon.com",
        "intro" => "A responsive wordpress Instagram contest for a famous Montreal brewery.",
        "content" =>"<p>This well known brewery needed a website but wanted to launch it through an Instragram contest. Had to program a plugin that could do just that. </p>",
        "tools"=>"Wordpress, jQuery, Instagram API",
        "share"=>"css/html + wordpress/javascript - everything but web design",
        "ribbon"=>"DEV"
    ),
    "lavitrine" =>
    array(
        "title" => "BLOG FOR LA VITRINE",
        "url" => "blogue.lavitrine.com",
        "intro" => "A repsonsive wordpress blog for a famous box office in Montreal.",
        "content" =>"<p>Customer via <a style='text-decoration:underline' target='_blank' href='http://http://www.tonton.ca/'>Tonton</a> wanted a responsive wordpress with many animations both CSS and Javascript along with a color scheme matching its categories. Many widgets were also required.
Tonton provided a very classy design to go with a savvy development!</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + wordpress/javascript – everything but web design",
        "ribbon"=>"DEV"
    ),
    "bleubonbon"=>array(
        "title" => "Bleu Bonbon",
        "url" => "bleubonbon.com",
        "intro" => "A responsive wordpress for a fun endeavour.",
        "content" =>"<p>That was fun ! Bleu Bonbon founder wanted a responsive and fresh wordpress that would befit his ideas and design ! Plus a mutliple-upload form using the best captcha ever : <a href='http://areyouahuman.com'>Are You A Humane ?</a>. I happened to be around ;)",
        "tools"=>"Wordpress, jQuery, Are You Human",
        "share"=>"css/html + wordpress/javascript - everything but web design",
        "ribbon"=>"DEV"
    ),
    "baron" =>
    array(
        "title" => "BARON",
        "url" => "baronmag.com",
        "intro" => "A full repsonsive Wordpress original theme for a popular Montreal publication",
        "content" =>"<p>Client needed a full responsive Wordpress publication website. I built the Wordpress theme from scratch using the provided design and had a blast in the process ! Particularities involved a full ad integration, gallery management and anything Wordpress would let you do.</p>",
        "tools"=>"Wordpress, jQuery",
        "share"=>"css/html + wordpress/javascript - everything but web design",
        "ribbon"=>"DEV"
    ),
/*    "adde" =>
    array(
        "title" => "ADDE",
        "url" => "adde.fr",
        "intro" => "Corporate website build with my own CMS",
        "content" =>"<p>For the customers content managing special needs,  no popular CMS would do. So I finally got to use my own handmade CMS for this Corporate website.
            <br>
            Enriched by a Raphael.js interactive map the result is a fine, perfectly designed - thank you <a href='http://atelier1n.com'>Mariane Laforest</a> - little big company showcase I am very proud of.</p>" ,
        "tools"=>"My very own Bakeware, jQuery, Raphael.js",
        "share"=>"css/html and development. Everything but web design!",
        "ribbon"=>"DEV++"
    ),*/
    "cssoff" =>
    array(
        "title" => "CSSOFF 2012 TOP 25",
        "url" => "unmatchedstyle.com/cssoff/2011/0291/",
        "url_show" => "check out my entry!",
        "intro" => "Scored big at the CSSOFF 2012 contest!",
        "content" =>"<p>I made 22nd ! ... out of 400+ at the <a style='text-decoration:underline;' href='http://unmatchedstyle.com/news/cssoff-winners-2012.php'>CSS OFF challenge</a> ! Special prize was given to the top 25.
            Entries had to be responsive, legacy browserish, php-free and of course very close to the initinal design in order to maximize points.
            <br>
            It's not work per se, just something I'm proud of, and so that goes into my portfolio.</p>" ,
        "tools"=>"CSS, jQuery",
        "share"=>"css/html + javascript - everything but design",
        "ribbon"=>"DEV"
    ),
/*    "candycookie" =>
    array(
        "title" => "CANDY COOKIE",
        "url" => "candycookieboulevard.fr/patisseries",
        "url_show" => "candycookieboulevard.fr",
        "intro" => "A blogging website for a famous coffee shop &amp; restaurant in France.",
        "content" =>"<p>The customer needed his current blogging plateform (tumblr) integrated into his website. His tumblr posts are grabbed via xml and fed directly into the code of the site.
<br>Customer’s social informations and publishing also had to be retrieved. (facebook last status + other numbers).
<br>Last request : A Flash free interactive food menu</p>",
        "tools" => "jQuery, xml, and php.",
        "share" => "css/html + php/javascript – everything but web design.",
        "ribbon"=>"DEV"
    ),*/
    "taxocanada" =>
    array(
        "title" => "TAX-O-CANADA",
        "url" => "taxocanada.com",
        "intro" => "A Canadian sale tax calculator plus its fancy map! ",
        "content" =>"<p>Tax-O-Canada calculate as you type the total sale taxes for the amount entered and the province selected. You can change province and immediately see the new total without reloading the page or retyping your amount.
                    <br>The language and locality of the user (his province when residing in canada) are detected on loading</p>",
        "tools" => "Raphael.js for the interactive map and jQuery for the rest.",
        "share" => "eve-ry-thing (but the logo).",
        "ribbon"=>"DESIGN + DEV"
    ),
/*    "cheribibi" =>
    array(
        "title" => "LES VIDEOS DE CHERIBIBI",
        "url" => "cheribibi-videos.com",
        "intro" => "A video content website with an innovative and stylish navigation.",
        "content" =>"<p>The customer wanted an innovative way to showcase his videos. We thought about one single page presenting all of his videos.
<br>For a better user experience, only the video currently running is fully displayed.
<br>On the click of a nib the contact form is neatly pulled down.
<br>A backoffice has been developped to allow the customer to add new videos when needed.</p>
",
        "tools" => "jQuery and PHP.",
        "share" => "css/html + php/javascript + backoffice&mdash;everything but web design.",
        "ribbon"=>"DEV"
    ),*/
/*    "iao" =>
    array(
        "title" => "I AM ORGANIZED&trade;",
        "url" => "iamorganized.com",
        "intro" => "Showcasing website for a launching clouding servive",
        "content" =>"<p>The client needed a simple but responsive multilingual website. Videos/Images had to fit according to the support's size. For the same purpose navigation adapts for mobile devices using PageSlide.js</p>",
        "tools" => "jQuery, php, FitVids.js, PageSlide.js",
        "share" => "css/html + php/javascript + design responsive.",
        "ribbon"=>"DEV"
    )*/

);

unset($works_en['candycookie'], $works_en['adde'], $works_en['griffon'], $works_en['readytodye']);
unset($works_fr['candycookie'], $works_fr['adde'], $works_fr['griffon'], $works_fr['readytodye']);
//SKILLS !
$quotes_en = array(
    //"pays" => "I come from France,  live in Montreal, Canada and speak those countries’ two great languages.",
    "pays" => "I come from France but live in Montreal, I'm fluent in French and English",
    "mysql" => "I love database projects! (as much as I love SQL).",
    "php" => "PHP is my first language and I&nbsp;love it very very much.",
    "wordpress" => "Wordpress is my best friend, but I can also build your own taylored CMS!",
    "mobile" => "Of course I do mobile web!",
    "css3" => "I'm css3 ready!",
    "socials" => "I can grab and style anything from your facebook, twitter, tumblr, whatever your social thingy - as long as it lets me.",
    "clowns" => "I’m afraid of clowns.<br> Yep. Sorry.",
    "html5" => "I'm HTML5 ready!",
    "webdesigners" => "I enjoy working with web designers and respect their expectations : no&nbsp;compromise.",
    "responsive" => "I’m all geeky about <a target='blank' href='http://www.alistapart.com/articles/responsive-web-design/'>Responsive Web Design</a>, usher the words and I’ll grin!",
    "love" => "My job evolves everyday, you must always say hello to new things and goodbye to old ones. That's what makes it so real and loveable.",
    "jquery" => "I’m a soldier of the jQuery revolution! (in power for a while now)",
    "toys" => "I love playing with new toys like raphael, modernizr, 960.gs but will only brag about them when needed.",
    "welcome" =>"What he can do,<br>what he likes to do,<br>blah blah blah,<br>click click click&hellip;",
);
$quotes_fr = array(
    "pays" => "Je viens de France mais vis à Montréal. Je parle courrement Français et Anglais.",
    "mysql" => "J'adore les projets à base de données (autant que j'aime SQL).",
    "php" => "PHP, c'est mon premier language, j'y suis très attaché.",
    "wordpress" => "Wordpress est mon meilleur ami, mais je peux aussi vous créer un CMS sur mesure !",
    "mobile" => "Bien sûr que je fais du web mobile&nbsp;!",
    "css3" => "Je suis prêt pour le CSS3&nbsp;!",
    "socials" => "Je peux récupérer n’importe quelle info de votre facebook, twitter, tumblr etc...<br>Je suis très &ldquo;social&rdquo;&nbsp;!",
    "clowns" => "J’ai peur des clowns. Ben ouais. Désolé.",
    "html5" => "Je suis prêt pour l'HTML5&nbsp;!",
    "webdesigners" => "Je connais bien les designers web et respecte leurs attentes&nbsp;: pas de compromis.",
    "responsive" => "Je suis fan du <a target='blank' href='http://www.alistapart.com/articles/responsive-web-design/'>Responsive Web</a>&nbsp;! Prononcez les mots et je deviens tout ému&nbsp;!",
    "love" => "Mon travail évolue tous les jours. Bonjour la nouveauté, au revoir l'obsolète. C'est un boulot 'vrai' et c'est pour ça que je l'aime.",
    "jquery" => "Je suis un soldat de la révolution jQuery (on a pris le pouvoir il y a bien longtemps).",
    "toys" => "J'adore m'amuser avec des nouveaux jouets comme raphael, modernizr, 960.gs mais ne m'en sers qu'au besoin.",
    "welcome" => "Ce qu'il sait faire,<br>ce qu'il aime faire,<br>bla bla bla,<br>clique clique clique&hellip;",
);

$errors_en = array(
    "javascript" => array(
        "header" => "Ok, by the look of it, you don't have Javascript enabled.",
        "content" => "<p>You're probably stuck in a coprorate environement where IT security is debating the future adoption of obsolete technologies&hellip;</p><p>Even though this website's basic functionalities will work without javascript enabled we strongly suggest you come back tonight from the comfort of you favorite javascript enabled navigator. Else <a href=\"no-js\">go&nbsp;ahead</a> and blame your company for the poor appearance of this website — not its makers.</p>"
    ),
    "oldbrowser" => array(
        "header" => "Woah ! That browser is old...",
        "content" => "<p>You’re either living in 1996 or stuck at work. This website has not been build with your old thing in mind. Come back tonight from the comfort of you prefered web browser or just <span>go ahead</span> and hate your browser even more. (you've been warned)</p>"
    )
);
$errors_fr = array(
    "javascript" => array(
        "header" => "Bon visiblement vous n'avez pas javascript activé",
        "content" => "<p>Vous êtes sûrement bloqué dans un environnement de travail où le conseil de sécurité tergiverse à l'adoption de technologies déjà obsolètes&hellip; </p><p>Même si les fonctionnalités basiques de ce site foncitonneront sans javascript, nous vous suggerons plutôt de revenir ce soir avec votre navigateur préféré et javascript. Sinon <a href=\"no-js\">allez-y</a> mais blamer votre entrepirse pour le maigre rendu du site&mdash;pas ses créateurs.</p>"
    ),
    "oldbrowser" => array(
        "header" => "Oula, ça c'est un vieux navigateur.",
        "content" => "<p>Ou vous vivez en 1996 ou alors vous êtes bloqué au travail. Ce site web n'a pas été conçu pour fonctionner avec votre navigateur. Revenez ce soir avec votre navigateur préféré ou alors <span>allez-y</span> et détester votre navigateur un peu plus. (on vous aura prévenu)</p>"
    )

);
?>