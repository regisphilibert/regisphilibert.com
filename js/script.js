$(function(){
    webroot = "/regisphilibert/"
    //MY OWN BG ANIMATION FUNCTION
    jQuery.fn.flipIt = function(time, size, frames){
            var i=1;
            var d = 0;
            while(i<=frames){
                $(this).delay(d).animate({'background-position':'-'+size*i+'px'}, 0)
                d=time;
                i++;
            }
    }
    jQuery.fn.unFlipIt = function(time, size, frames){

            var i=1;
            var d = 0;
            while(i<=frames+1){
                $(this).delay(d).animate({'background-position':size*i+'px'}, 0)
                d=time;
                i++;
            }
    }

    if(location.href.search("no-js")!= "-1"){
     document.location.pathname=webroot;
}
});

$(document).ready(function(){

$()
  $(window).load(function(){
     $('.top, .bottom, .blank, .main').each(function(index){
        var dash = $(this).attr("class")=="main"?"dashwhite":"dashcolor";
        pos = $(this).position();
        pos = pos.top;
        $('body').append('<div class="'+dash+' '+index+'"></div>')
        $('.'+dash+'.'+index).css({top:pos-1});
    })
  })
//BROWSER SNIFFER so IE6 and lower users know what they're up against.
if($.browser.msie){
      if($.browser.version < 7){
         $('#old_error').show();
         $("html, body").css("overflow", "hidden");
        $('.warning p span').click(function(){
            $(this).parents('.warning').fadeOut(300);
            $("html, body").css("overflow", "auto");
        })
      }
}

//MAKING SURE EVERYTHING's SMOOTH FOR JS ENABLED USERS (php takes care of JS DISABLED users)
$('#about .main a, .badge, .worknav a').removeAttr("href");

//THE MAIN STUFF
    //THE MOUSE
    //MOUSE ANIMATION  ON HOVER
    $('.top .left').not('#about .top .left').hover(
        function(){
            $(this).find('.mouse').stop(true,true).flipIt(30, 39, 8);
            },
        function(){
            $(this).find('.mouse').stop(true,true).unFlipIt(30,39,8);
        }
    )
    //MOUSE SCROLL TO TOP
    $('.top .left').not('#about .top .left').click(function(){
        that = $(this).parent();
        that.find('.dialog').html('<span>bye bye</span>').show("slide", {direction:'left'}, 30*8);
        setTimeout(
            function(){$('html, body').animate({scrollTop: that.offset().top-40}, 500,
                        function(){
                            $('html, body').animate({scrollTop: 0}, 500)
                            $('.dialog').hide( ).html();
                        })
            }
            ,750
        )
    })
    //DRAWS HORIZ DASH LINES

    //SET WAYPOINTS thanks to waypoint.js
    $('#about, #skills, #works, #contact').waypoint(function() {

        id = $(this).attr('id');
        $('#menu > .right > div.active').removeClass('active');
        $('#menu > .right > div > a[title='+id+']').parent().addClass('active');
    },
    {offset:45}
    );
    $('.bottom').waypoint(function() {

        id = $(this).parent().attr('id');
        $('#menu > .right > div.active').removeClass('active'), 2000,
        function(){$('#menu > .right > div[title='+id+']').addClass('active', 2000);}

    }
    );


//#MENU
    //MENU AND SCROLL NAVIGATION
    $('#menu').find('a').add('#about a').click(function(){
        zone = $(this).attr('title');
        zone = $('#'+zone).offset().top
        var off = $.browser.mozilla?38:39;//There's a 1px diff in FF's offset() :/
        $('html, body').stop(true,true).animate({
            scrollTop: zone-off
            }, 1000);
        return false;
    })
    //MENU A hover state on #ABOUT A hovering
     $('#about a').hover(
        function(){
            name = $(this).attr('title');
            $('#menu .right a[title='+name+']').toggleClass("hovered");
        }
    )

//#WORKS
    //HIDE ALL BUT INIT
    $('.work').not('.init').hide()

    //WORKS NAVIGATION

    var s = 500;
    var anim = false;
    $('.worknav_right').click(function(){
        if(anim == false){
            anim = true;
            var active = $('#works .main .active');
            var next =  active.next('.work').length ? active.next('.work') : $('#works .main').children('.work').first();
            active.stop(true,true).hide("slide", {direction:'left'}, s);
            var name = next.attr('title');
            $('.worknav span').html(name).show("slide", {direction:'right'}, s)
            next.addClass('active')
            .stop(true, true)
            .show("slide", {direction:'right'}, s, function(){
                active.removeClass('active');
                anim = false;
                })
        }
        return false;
    })
    $('.worknav_left').click(function(){
        if(anim == false){
            anim = true;
            var active = $('#works .main .active');
            var prev =  active.prev('.work').length ? active.prev('.work') : $('#works .main').children('.work').last();
            active.hide("slide", {direction:'right'}, s);
            var name = prev.attr('title');
            $('.worknav span').html(name).show("slide", {direction:'left'}, s)
            prev.addClass('active')
            .show("slide", {direction:'left'}, s, function(){
                active.removeClass('active');
                anim = false;
                })
        }
        return false;
    })

//#SKILLS
    //CLICK on BADGES
        $('.quote').not('.quote[title="welcome"]').hide();
        $('.badge').click(function(){
                s=300;
                which = $(this).attr('title');
                $('.quote[title="'+which+'"]').siblings().stop(true,true).fadeOut(s).delay(s).end().stop(true,true).fadeIn(s);
                return false;
        })




//#CONTACT
    //YELLOW BACKDROP ON HOVER (!CSSTRANSITION FALLBACK)
    if(!Modernizr.csstransitions){
        $('#contact .left a').hover(
                function(){
                    that=$(this).find('.yellow');
                    that.a = $(this).find('.link');
                    that.stop(true, true).animate({height:'100%'},300)
                    that.a.stop(true,true).delay(150).animate({color:'#fff'}, 50);
                    },
                function(){
                    that=$(this).find('.yellow');
                    that.a = $(this).find('.link');
                    that.stop(true, true).animate({height:0},300)
                    that.a.stop(true,true).delay(50).animate({color:'#44b6ae'}, 50);
                    }
    )
    }

    //The chop email function to fight spammer, not sure how safe it is.
    function chopMail(email){
        choped = email.split();
        joined = choped.join();
        return joined;
    }
    $('#contact .mail .link').html(chopMail('bonjour@regisphilibert.com'));
    $('#contact .mail').attr('href', 'mailto:'+chopMail('bonjour@regisphilibert.com'))


    $('.chevrons').hide()

if(!Modernizr.touch){//THE CHEVRONS ONLY IF NOT TOUCH

$('.submit').hover(
            function(){
                chevrOn = function(){
                    delay = 0;
                    $('.chevrons span').css({opacity:0.3});
                    $('.chevrons span').each(function(){
                        $(this).delay(delay).animate({opacity:1},50).delay(50).animate({opacity:0.3});
                        delay += 50;
                    })
                }
                number = $('.chevrons span').length;
                $('.chevrons').show(0, function(){chevrOn();stop = setInterval(chevrOn, number*50+400);})
            },
            function(){
                $('.chevrons').hide(0, function(){
                     $('.chevrons span').stop(true, true);
                    clearInterval(stop);
                })

            }
    );
}


})