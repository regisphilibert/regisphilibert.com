<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<link rel="dns-prefetch" href="//ajax.googleapis.com" />
	<link rel="dns-prefetch" href="//regisphilibert.com" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php
$desc_en = array(
	"title"=> "Regis Philibert - freelance web developer",
	"desc" => "Hey I build websites for every budget and every ideas. I live in Europe but work with many countries. Contact me through this porfowebsite of mine!",
);
$desc_fr = array(
	"title"=> "Regis Philibert - developpeur web freelance",
	"desc" => "Hey, je fabrique des sites pour tous les budgets et toutes les attentes. J'habite en Europe mais travaille avec le monde entier. Découvrez en plus avec mon site/portfolio",
);
$desc = $set_lang=="fr"?$desc_fr:$desc_en;
$og = array(
	"title"=>"Regis Philibert &mdash; web developer",
	"description"=>"I build websites and this is my porfowebsite | Je fabrique des sites web et voici mon porfositeweb!",
	"logo"=>"http://www.regisphilibert.com/logo.png",
	"url"=>"http://www.regisphilibert.com"
);
if($w_url){
	$og = array(
	"title"=>"Regis Philibert &mdash; web developer",
	"description"=>"A new addition to my portoflio: ".$works_en[$w_url]['title'],
	"logo"=>"http://www.regisphilibert.com/img/works/".$w_url."_ss.jpg",
	"url"=>"http://www.regisphilibert.com/".$w_url."#works"
);
}
?>

	<title><?php echo $desc['title'] ?></title>
	<meta name="description" content="<?php echo $desc['desc'] ?>">
	<meta name="author" content="Regis Philibert">

	<meta name="google-site-verification" content="fy1MqpS8s6-PgMfwS3F3kJGcKmo2g-SrqY0Nrj6lJAM" />

	<meta name="msapplication-navbutton-color" content="#b3e4de" />

	<meta property="og:title" content="<?php echo $og['title'] ?>" />
	<meta property="og:description" content="<?php echo $og['description'] ?>" />
	<meta property="og:image" content="<?php echo $og['logo'] ?>" />
	<meta property="og:url" content="<?php echo $og['url'] ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Regis Philibert" />
	<meta property="og:type" content="website">
	<meta property="fb:admins" content="569412883" />

	<!--<meta name="viewport" content="width=480, initial-scale=1">-->

	<link rel="shortcut icon" href="favicon.ico">

	<link rel="stylesheet" href="<?php echo WEBROOT ?>style.css?v=2">

	<script src="<?php echo WEBROOT ?>js/libs/modernizr-1.7.min.js"></script>
</head>

<body>
	<div class="warning" id="old_error">
		<div class="warning_container">
			<p class="face">:/</p>
			<h3><?php echo $err_message['oldbrowser']['header'] ?></h3><?php echo $err_message['oldbrowser']['content'] ?>
			<p class="warning_lang"><a href="<?php echo WEBROOT ?>fr">francais</a>, <a href="<?php echo WEBROOT ?>en">english</a></p>
		</div>
	</div>
	<div id="container">
		<?php include "menu.php" ?>
		<?php include "about.php" ?>
		<?php include "works.php" ?>
		<?php include "skills.php" ?>
		<?php include "contact.php" ?>
	</div>
	<?php include "footer.php" ?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>

	<script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.5.1.min.js"%3E%3C/script%3E'))</script>
	<script src="<?php echo WEBROOT ?>js/libs/waypoints.min.js"></script>
	<script src="<?php echo WEBROOT ?>js/libs/jquery.coloranimations.js"></script>
	<script src="<?php echo WEBROOT ?>js/plugins.js"></script>
	<script src="<?php echo WEBROOT ?>js/formval.js"></script>
	<script src="<?php echo WEBROOT ?>js/script.js"></script>


	<!--[if lt IE 7 ]>
	<script src="<?php echo WEBROOT ?>js/libs/dd_belatedpng.js"></script>
	<script> DD_belatedPNG.fix('img, .png_bg');</script>
	<![endif]-->
	<script>
		var _gaq=[['_setAccount','UA-24416415-1'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
</body>
</html>