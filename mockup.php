
<div id="menu">
		<div class="left"></div>
		<div class="right">
				<div class="nav_box active"><a href="#about" name="about">ABOUT</a></div>
				<div class="nav_box"><a href="#works" name="works">WORKS</a></div>
				<div class="nav_box"><a href="#skills" name="skills">WHY ME</a></div>
				<div class="nav_box"><a href="#contact" name="contact">HIRE ME</a></div>
		</div>
		<ul class="lang">
			<li class="active"><a>En</a></li>
			<li><a>Fr</a></li>
		</ul>
</div>
<div class="top_margin">
	<div class="left"></div>
	<div class="right">
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
	</div>
</div>

<!-- ///////////////////// ABOUT ////////////////////////-->
<div id="about">
	<div class="top">
		<div class="dialog"><span>Lorem atchoum Siclamed</span></div>
		<div class="left">
			<div class="mouse"></div>
			<div class="logo"><img src="img/logo.png"/></div>
			<div class="baseline">DEVELOPPEUR WEB<br>FRONT &amp; BACK</div>
		</div>
		<div class="right">
			<div class="nav_box arrow"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
	</div>
	
	<div class="main">
		<p><strong>Hey I build websites.</strong> I’m Regis Philibert, web developer, front & back, browser optimizer, social querier, cms welcomer. <a name="works">Been there</a>, <a name="skills">done that</a>. Weither you already have your <del>designs</del> psd or just ideas, <a name="contact">leave me a note</a> so we can build them!</p>
	</div>
		
	<div class="bottom">
		<div class="left">
			<div class="socials">
				<div>
					<span>YOU CAN ALSO FIND ME HERE</span>
				</div>
				<ul>
					<li>
						<a class="tw"></a>
					</li>
					<li>
						<a class="in"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
	</div>
</div>
<!-- ///////////////////// TUOBA  ////////////////////////-->
<div class="blank">
		<div class="left"></div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
		
</div>
<!-- ///////////////////// WORKS  ////////////////////////-->
<div id="works">
	<div class="top">
		<div class="dialog"><span>Lorem atchoum Siclamed sur deux lignes, yeah !</span></div>
		<div class="left">
			<div class="mouse"></div>
			<div class="logo"><img src="img/logo.png"/></div>
			<div class="baseline">DEVELOPPEUR WEB<br>FRONT & BACK</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box arrow"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
	</div>
	
	<div class="main">
		<div class="worknav">
				<a class="worknav_left"></a><span>CANDYCOOKIE BOULEVARD</span><a class="worknav_right"></a>
		</div>
		<div class="work init active" name="CANDYCOOKIE BOULEVARD">
			<div class="left">
				<img src="img/candycookie_logo.png"/>
				<p class="intro">Un site vitrine pour un salon de thé et restaurant très en vue...</p>
				<p>Le client souhaitait pouvoir blogger sans pour autant changer de pateforme. J'ai donc integré son tumblr directement dans son site.
				<br>On récupère aussi les informations liées à ses comptes "sociaux", tout comme son dernier statut facebook livré dans le footer.
				<br>Le client voulait de simples animations mais sans flash!
				</p>
	
				
				</p>
				<p class="brandlink"><a target="blank" href="http://candycookieboulevard.fr/patisseries">candycookieboulevard.fr</a></p>
			</div>
			<div class="right">
				<img src="img/candycookie_ss.jpg"/>
			</div>
		</div>
		<div class="work" name="LA VIDA LOCA">
			<div class="left">
				<img src="img/candycookie_logo.png"/>
				<p class="intro">Two enormous wooden pots painted black, and suspended by asses' ears, swung from the cross-trees of an old top-mast, planted in front of an old doorway. The horns of the cross-trees were sawed.</p>
				<p>Two enormous wooden pots painted black, and suspended by asses' ears, swung from the cross-trees of an old top-mast, planted in front of an old doorway. The horns of the cross-trees were sawed.</p>
				<p class="brandlink"><a href="http://candycookieboulevard.fr">lavidaloca.es</a></p>
			</div>
			<div class="right">
				<img src="img/candycookie_ss.jpg"/>
			</div>
		</div>
		<div class="work" name="MY THIRD.COM">
			<div class="left">
				<img src="img/candycookie_logo.png"/>
				<p class="intro">Two enormous wooden pots painted black, and suspended by asses' ears, swung from the cross-trees of an old top-mast, planted in front of an old doorway. The horns of the cross-trees were sawed.</p>
				<p>Two enormous wooden pots painted black, and suspended by asses' ears, swung from the cross-trees of an old top-mast, planted in front of an old doorway. The horns of the cross-trees were sawed.</p>
				<p class="brandlink"><a href="http://candycookieboulevard.fr">mythird.3rd</a></p>
			</div>
			<div class="right">
				<img src="img/candycookie_ss.jpg"/>
			</div>
		</div>
	</div>
		
	<div class="bottom">
		<div class="left">
			<div class="socials">
				<div>
					<span>YOU CAN ALSO FIND ME HERE</span>
				</div>
				<ul>
					<li>
						<a class="tw"></a>
					</li>
					<li>
						<a class="in"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
	</div>
</div>
<!-- ///////////////////// SKROW  ////////////////////////-->
<div class="blank">
		<div class="left"></div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
			<div class="nav_box"></div>
		</div>
		
</div>
<!-- ///////////////////// SKILLS  ////////////////////////-->
<div id="skills">
	<div class="top">
		<div class="dialog"><span>Lorem atchoum Siclamed</span></div>
		<div class="left">
			<div class="mouse"></div>
			<div class="logo"><img src="img/logo.png"/></div>
			<div class="baseline">DEVELOPPEUR WEB<br>FRONT & BACK</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box arrow"></div>
			<div class="nav_box"></div>
		</div>
	</div>
	
	<div class="main">
		<div class="left">
			<div class="badge c01" name="pays"></div>
			<div class="badge c02" name="mysql"></div>
			<div class="badge c03" name="php"></div>
			<div class="badge c04" name="wordpress"></div>
			<div class="badge c05" name="mobile"></div>
			<div class="badge c06" name="css3"></div>
			<div class="badge c07" name="socials"></div>
			<div class="badge c08" name="clowns"></div>
			<div class="badge c09" name="html5"></div>
			<div class="badge c10" name="webdesigners"></div>
			<div class="badge c11" name="responsive"></div>
			<div class="badge c12" name="love"></div>
			<div class="badge c13" name="jquery"></div>
			<div class="badge empty01"></div>
			<div class="badge empty02"></div>
			<div class="badge empty03"></div>
		</div>
		<div class="right">
			<div class="quote welcome">
					<p>What he can do,<br>what he likes to do,<br>blah blah blah,<br>click click click&hellip;</p>
				<div class="between"></div>
				<img src="img/skills/mouse.png"/>
			</div>
			<div class="quote" name="pays">
				<p>I come from France,  live in Canada and speak those countries’ 2 great languages.</p>
				<div class="between"></div>
				<img src="img/skills/pays.png"/>
			</div>
			<div class="quote" name="mysql">
				<p>I’m specialised in front end dev but also manages everything in the back.</p>
				<div class="between"></div>
				<img src="img/skills/mysql.gif"/>
			</div>
			<div class="quote" name="php">
				<p>PHP is my strong suit : Regis Philibert, Ph. P.</p>
				<div class="between"></div>
				<img src="img/skills/php.gif"/>
			</div>
			<div class="quote" name="wordpress">
				<p>Wordpress is my friend, not an automatic response.</p>
				<div class="between"></div>
				<img src="img/skills/wordpress.gif"/>
			</div>
			<div class="quote" name="mobile">
				<p>Of course I do mobile web!</p>
				<div class="between"></div>
				<img src="img/skills/mobile.gif"/>
			</div>
			<div class="quote" name="css3">
				<p>I'm css3 ready!</p>
				<div class="between"></div>
				<img src="img/skills/css3.gif"/>
			</div>
			<div class="quote" name="socials">
				<p>I can grab and style anything from your facebook, twitter, tumblr, whatever your social thingy - as long as it lets me.</p>
				<div class="between"></div>
				<img src="img/skills/socials.gif"/>
			</div>
			<div class="quote" name="clowns">
				<p>I’m afraid of clowns.<br> Yep. Sorry.</p>
				<div class="between"></div>
				<img src="img/skills/clowns.gif"/>
			</div>
			<div class="quote" name="html5">
				<p>I'm HTML5 ready !</p>
				<div class="between"></div>
				<img src="img/skills/html5.gif"/>
			</div>
			<div class="quote" name="webdesigners">
				<p>I enjoy working with web designers and respect their expectations : no compromise.</p>
				<div class="between"></div>
				<img src="img/skills/webdesigners.gif"/>
			</div>
			<div class="quote" name="responsive">
				<p>I’m all geeky about Responsive Web Design, usher the words and I’ll grin!</p>
				<div class="between"></div>
				<img src="img/skills/responsive.gif"/>
			</div>
			<div class="quote" name="love">
				<p>I love making websites because their building process evolves every day, and so does my job :)</p>
				<div class="between"></div>
				<img src="img/skills/love.gif"/>
			</div>
			<div class="quote" name="jquery">
				<p>I’m a soldier of the jQuery revolution! (in power for a while now)</p>
				<div class="between"></div>
				<img src="img/skills/jquery.gif"/>
			</div>
			
			
		</div>
	</div>
		
	<div class="bottom">
		<div class="left">
			<div class="socials">
				<div>
					<span>YOU CAN ALSO FIND ME HERE</span>
				</div>
				<ul>
					<li>
						<a class="tw"></a>
					</li>
					<li>
						<a class="in"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
		</div>
	</div>
</div>
<!-- ///////////////////// SLLIKS  ////////////////////////-->
<div class="blank">
		<div class="left"></div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box"></div>
		</div>
</div>
<!-- ///////////////////// CONTACT  ////////////////////////-->
<div id="contact">
	<div class="top">
		<div class="dialog"><span>Lorem atchoum Siclamed</span></div>
		<div class="left">
			<div class="mouse"></div>
			<div class="logo"><img src="img/logo.png"/></div>
			<div class="baseline">DEVELOPPEUR WEB<br>FRONT & BACK</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box arrow"></div>
		</div>
	</div>
	
	<div class="main">
		<div class="left">
			<div class="text">
				<p>Lorem ipsum is simply dummy text of the printing and typesetting industry.</p>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
				
			</div>
			<a class="skype">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">regisphilibert</p>
			</a>
			<a href="" class="mail">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">hail < at > regisphilibert.com</p>
			</a>
			<a class="phone">
				<div class="yellow"></div>
				<div class="content"></div>
				<p class="link">+1 (514) 555 4545</p>
			</a>
		</div>
		<div class="right">
			
			<form action="process.php" method="post">
				<label for="fName">NAME <?php if(in_array('name', $errors)):?><span content="C'est pas un nom ça">/C'est pas un nom ça/</span><?php endif; ?></label>
				<input type="text" name="name" id="fName"/>
				<label for="fMail">EMAIL <?php if(in_array('mail', $errors)):?><span content="C'est pas un mail ça">/C'est pas un mail ça/</span><?php endif; ?></label>
				<input type="text" name="mail" id="fMail"/>
				<label for="fObject">OBJECT</label>
				<input type="text" name="object" id="fObject"/>
				<label for="fMessage">MESSAGE  <?php if(in_array('message', $errors)):?><span content="Vous écrirez bien un petit quelque chose ?">/Vous écrirez bien un petit quelque chose ?/</span><?php endif; ?></label>
				<textarea name="message" id="fMessage"></textarea>
				<div class="submit">
					<button type="submit" id="submit_button">ENVOYER</button>
					<div class="chevrons"><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span><span>></span></div>
					
					
				</div>
			</form>
			<div class="form_printed">
				<h2>THANK YOU</h2>
				<p class="fName"><span class="title">NAME: </span><span class="content">John P. Ryley</span></p>
				<p class="fMail"><span class="title">COMPANY: </span><span class="content">Big Payer Inc.</span></p>
				<p class="fObject"><span class="title">OBJECT: </span><span class="content">Project inquiry</span></p>
				<p class="fMessage"><span class="title">MESSAGE: </span><span class="content">In 1778, a fine ship, the Amelia, fitted out for the express purpose, and at the sole charge of the vigorous Enderbys, boldly rounded Cape Horn, and was the first among the nations to lower a whale-boat of any sort in the great South Sea.</span></p>
			</div>
		</div>
	</div>
		
	<div class="bottom">
		<div class="left">
			<div class="socials">
				<div>
					<span>YOU CAN ALSO FIND ME HERE</span>
				</div>
				<ul>
					<li>
						<a class="tw"></a>
					</li>
					<li>
						<a class="in"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
		</div>
	</div>
</div>
<!-- ///////////////////// TCATNOC  ////////////////////////-->
<div class="blank">
		<div class="left"></div>
		<div class="right">
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
			<div class="nav_box none"></div>
		</div>
</div>

<!-- ///////////////////// FOOTER  ////////////////////////-->