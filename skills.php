<?php $page ="skills";
$quotes = $set_lang == "fr"?$quotes_fr:$quotes_en;

?>
<!-- ///////////////////// SKILLS ////////////////////////-->
<div id="skills">
	<?php include "blocks/top.php" ?>
<div class="main">
<div class="left">
        <?php
        $c = 01;
        foreach($quotes as $k => $v){
            $c_ts=$c<10?"0".$c:$c;// echo int 01 echoes "1" instead of "01". hence this little trick.
            if($k!="welcome"){ ?>
                <a class="badge c<?php echo $c_ts ?>" title="<?php echo $k ?>" <?php echo 'href="'.WEBROOT.'?s='.$k.'#skills"' ?>></a>
            <?php
        $c++;    
        }?>   
        <?php }
        ?>
        <a class="badge empty01"></a>
        <a class="badge empty02"></a>
</div>
<div class="right">
        <?php if(!isset($_GET['s'])){
        
        foreach($quotes as $k => $v){ ?>
        <table class="quote <?php echo $k=="welcome"?"welcome":""?>" title="<?php echo $k ?>">
                <tr><td class="content"><?php echo $v ?></td></tr>
                <tr><td class="between"></td></tr>
                <tr><td class="image"><img alt="badge/<?php echo $k ?>" src="img/skills/<?php echo $k ?>.gif"/></td></tr>
        </table>
        <?php }
        }
        else{
        $a_quote = $_GET['s']
        ?>
        <table class="quote" title="<?php echo $a_quote ?>">
                <tr><td class="content"><?php echo $quotes[$a_quote] ?></td></tr>
                <tr><td class="between"></td></tr>
                <tr><td class="image"><img alt="badge/<?php echo $a_quote ?>" src="img/skills/<?php echo $a_quote ?>.gif"/></td></tr>
        </table>    
        <?php } ?> 
</div>
</div>
	<?php include "blocks/bottom.php" ?>
</div>
<!-- ///////////////////// END SKILLS  ////////////////////////-->
<?php include "blocks/blank.php" ?>