<?php $page ="works";
$works = $set_lang!="fr"?$works_en:$works_fr;
?>
<!-- ///////////////////// WORKS ////////////////////////-->

<div id="works">
	<?php include "blocks/top.php" ?>
        <?php
        $keys_w = array_keys($works);
        $active_w = isset($_GET['w'], $works) ? $_GET['w'] : $keys_w[0];
        $active_w_data = $works[$active_w];
        $next = array_search($active_w, $keys_w)==count($keys_w)-1?0:array_search($active_w, $keys_w)+1;
        $prev = array_search($active_w, $keys_w)==0?count($keys_w)-1:array_search($active_w, $keys_w)-1;
        ?>
        <?php if(!isset($_GET['w'])):
        $init = $w_url ? $w_url : "artpublic";
        ?>
        <div class="main">
		<div class="worknav">
			<a class="worknav_left" href="<?php echo WEBROOT."?w=".$keys_w[$prev]."#works";?>"><?php echo _('previous') ?></a><span><?php echo $w_url ? $works_fr[$w_url]['title'] : $works[$init]['title']; ?></span><a class="worknav_right" href="<?php echo WEBROOT."?w=".$keys_w[$next]."#works";?>"><?php echo _('next') ?></a>
		</div>
		<?php

                foreach($works as $k => $v){ extract($v) ;

				?>
                    <div class="work <?php echo $k==$init?"init active":"" ?>" title="<?php echo $title ?>">
			<div class="left">
				<img alt="Logo/<?php echo $title ?>" src="<?php echo WEBROOT ?>img/works/<?php echo $k ?>_logo.png"/>
				<p class="intro"><?php echo $intro ?></p>
				<?php echo $content ?>
                                <p><strong><?php echo _("My tools:") ?></strong> <?php echo $tools ?>
                                    <br><strong><?php echo _("My share:") ?></strong> <?php echo $share ?>
                                </p>
				<p class="brandlink"><a target="_blank" rel="nofollow" href="http://<?php echo $url; ?>"><?php echo isset($url_show)?$url_show:$url; ?></a></p>

			</div>
			<div class="right">
				<div class="ribbon"><span><?php echo $ribbon ?></span></div>
				<img alt="Screenshot/<?php echo $title ?>" src="<?php echo WEBROOT ?>img/works/<?php echo $k ?>_ss.jpg"/>

			</div>
                    </div>
                <?php
                unset($url_show);
                }
                ?>
	</div>
        <?php else:
        extract($active_w_data);

        ?>
        <div class="main">
            <div class="worknav">
		    <a class="worknav_left" href="<?php echo WEBROOT."?w=".$keys_w[$prev]."#works";?>"><?php echo _('previous') ?></a><span><?php echo $title ?></span><a class="worknav_right" href="<?php echo WEBROOT."?w=".$keys_w[$next]."#works";?>"><?php echo _('next') ?></a>
	    </div>
            <div class="work" title="<?php echo $title ?>">
			<div class="left">
				<img alt="Logo/<?php echo $title ?>" src="<?php echo WEBROOT ?>img/works/<?php echo $active_w ?>_logo.png"/>
				<p class="intro"><?php echo $intro ?></p>
				<?php echo $content ?>
                                <p><strong><?php echo _("My tools:") ?></strong> <?php echo $tools ?>
                                    <br><strong><?php echo _("My share:") ?></strong> <?php echo $share ?>
                                </p>
				<p class="brandlink"><a target="_blank" rel="nofollow" href="http://<?php echo $url; ?>"><?php echo isset($url_show)?$url_show:$url; ?></a></p>
			</div>
			<div class="right">
				<div class="ribbon"><span><?php echo $ribbon ?></span></div>
				<img alt="Screenshot/<?php echo $title ?>" src="<?php echo WEBROOT ?>img/works/<?php echo $active_w ?>_ss.jpg"/>
			</div>
                    </div>
        </div>
        <?php endif; ?>
	<?php include "blocks/bottom.php" ?>
</div>
<!-- ///////////////////// END WORKS  ////////////////////////-->
<?php include "blocks/blank.php" ?>