<?php

?>
<div class="bottom">
		<div class="left">
			<div class="socials">
				<div>
					<span><?php echo _("YOU CAN ALSO FIND ME HERE") ?></span>
				</div>
				<ul>
					<li>
						<a target="blank" href="https://twitter.com/regisphilibert" class="tw"></a>
					</li>
					<li>
						<a target="blank" href="http://www.linkedin.com/in/regisphilibert/<?php echo $set_lang!="fr"?"en":""; ?>" class="in"></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="right">
			<div class="nav_box <?php echo arrowBottom($page, 1) ;?>"></div>
			<div class="nav_box <?php echo arrowBottom($page, 2) ;?>"></div>
			<div class="nav_box <?php echo arrowBottom($page, 3) ;?>"></div>
			<div class="nav_box <?php echo arrowBottom($page, 4) ;?>"></div>
		</div>
</div>