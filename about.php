<?php $page ="about"; ?>
<!-- ///////////////////// ABOUT ////////////////////////-->
<div id="about">
	<?php include "blocks/top.php" ?>
	<div class="main">
            <?php if($set_lang == "fr"):?>
                <p><strong>Je construis des sites web.</strong> Je suis Régis Philibert, je développe pour le web, optimise pour les navigateurs, alimente de &ldquo;social&rdquo; et habille d’un cms au besoin. Voilà <a title="works" href="<?php echo WEBROOT.'#works';?>">mon travail</a> et <a title="skills" href="<?php echo WEBROOT.'#skills';?>">ma méthode</a>. Que vous ayez déjà un <del>design</del> psd ou juste des idées, <a title="contact" href="<?php echo WEBROOT.'#contact';?>">contactez-moi</a> qu’on s’en occupe ensemble&nbsp;!</p>
                <?php else: ?>
                <p><strong>Hey I build websites.</strong> I’m Regis Philibert, web developer, browser optimizer, social querier, cms welcomer. Here are <a title="works" href="<?php echo WEBROOT.'#works';?>">my works</a> and <a title="skills"  href="<?php echo WEBROOT.'#skills';?>">methods</a>. Whether you already have your <del>designs</del> psd or just ideas, <a title="contact"  href="<?php echo WEBROOT.'#contact';?>">leave me a note</a> so we can build them!</p>
                <?php endif; ?>
	</div>
	<?php include "blocks/bottom.php" ?>
</div>
<!-- ///////////////////// END ABOUT  ////////////////////////-->
<?php include "blocks/blank.php" ?>