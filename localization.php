<?php
// define constants
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR .'/locale');
define('DEFAULT_LOCALE', 'en');

require_once('lib/gettext/gettext.inc');
$supported_locales = array('fr', 'en');
$encoding = 'UTF-8';

$locale = $set_lang?$set_lang : DEFAULT_LOCALE;
// gettext setup
$locale = $locale=="fr"?"fr_FR":$locale;//<---------- OH POURRI CA !

putenv("LC_MESSAGES=$locale");
T_setlocale(LC_MESSAGES, $locale);
// Set the text domain as 'messages'
$domain = 'rp';
bindtextdomain($domain, LOCALE_DIR);
// bind_textdomain_codeset is supported only in PHP 4.2.0+
if (function_exists('bind_textdomain_codeset')) 
  bind_textdomain_codeset($domain, $encoding);
textdomain($domain);

header("Content-type: text/html; charset=$encoding");
?>