<?php
if(isset($_POST)){
    extract($_POST);
    $valid = true;
    $errors = array();
    if(empty($name)){
        $valid = false;
        $errors[] = "name";
    }
    //validate email address is not empty
    if(empty($email)){
        $valid = false;
        $errors[] = "email";
    //validate email address is valid
    }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        $valid = false;
        $errors[] = "email";
    }

    //validate message is not empty
    if(empty($message)){
        $valid = false;
        $errors[] = "message";
    }
    if($age != ''){
        $valid = false;
    }


    if($valid){
        $headers = "From: bonjour@regisphilibert.com" . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $notice = "<p>Un contact depuis regisphilibert.com</p>
                        <p>Language: </strong> {$locale} </p>
                      <p><strong>Name: </strong> {$name} </p>
                      <p><strong>Email Address: </strong> {$email} </p>
                      <p><strong>Object: </strong> {$object} </p>
                      <p><strong>Message: </strong> {$message} </p>";
        $emaiToSend_en = array(
            "emailbody" => "<p>Hey! Thanks for your touching note. I'll get back to you asap.</p>
                        <p><strong>Belows's what you left me:</strong>
                        <p>Dear Regis...</p>
                      <p><strong>Name: </strong> {$name} </p>
                      <p><strong>Email: </strong> {$email} </p>
                      <p><strong>Object: </strong> {$object} </p>
                      <p><strong>Message: </strong> {$message} </p>",
            "emailobject" => "Thanks! Regis Philibert got your note."
        );
        $emaiToSend_fr = array(
            "emailbody" => "<p>Bonjour et merci pour votre message si touchant! Je vous répondrai dès que possible.</p>
                    <p>Voilà ce que vous m'avez laissé.</p>
                    <p>Cher Régis...</p>
                      <p><strong>Nom: </strong> {$name} </p>
                      <p><strong>Courriel: </strong> {$email} </p>
                      <p><strong>Objet: </strong> {$object} </p>
                      <p><strong>Message: </strong> {$message} </p>",
            "emailobject" => "Merci! Regis Philibert a eu votre message."
        );
        $mailto = $locale!="fr"?$emaiToSend_en:$emaiToSend_fr;
        mail("bonjour@regisphilibert.com","Un contact sur regisphilibert.com",$notice,$headers);
        mail($email, $mailto['emailobject'],$mailto['emailbody'], $headers);
    }

        $returndata = array(
            'posted_form_data' => array(
                'name' => $name,
                'email' => $email,
                'object' => $object,
                'message' => $message
            ),
            'valid' => $valid,
            'errors' => $errors
        );

    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest'){
        //set session variables
        session_start();
        $_SESSION['cf_returndata'] = $returndata;

        //redirect back to form
        header('location: ' . $_SERVER['HTTP_REFERER'].'#contact');
    }
}

?>